import socket
import os
import msvcrt
hit = 0
width = os.get_terminal_size().columns
name = input("please enter your name \n")
print("********************************************************\n".center(width))
print(name.center(width))
print("********************************************************\n".center(width))
#198.143.180.88 is my vps IP that the server is run on it
def connect(TCP_IP,TCP_PORT):
    TCP_IP = TCP_IP
    TCP_PORT = TCP_PORT
    BUFFER_SIZE = 1024
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.1)
    connect = False
    try:
        s.connect((TCP_IP, TCP_PORT))
        print("you are now connected to the server")
        connect = True
    except :
        print("server not found")
        quit()
    return s
def mysend(socket,data):
    try:
        socket.send(data)
    except:
        print("sorry but you are disconnected")
        quit()

s = connect('127.0.0.1',5005)
clientMode = input("select the client mode:"
                   "\n1-type in your own message 2-view other user message\n")

mysend(s,bytes("mode"+clientMode+name, 'UTF-8'))

while connect:
    hit = msvcrt.kbhit()
    if clientMode == '1' :
        message_to_send = input("type your message:\n")
        if message_to_send == "#view":
            clientMode = '2'
            mysend(s,bytes("mode" + clientMode, 'UTF-8'))
        elif message_to_send == "#quit":
            mysend(s,bytes("left", 'UTF-8'))
            connect = False
        else:
            mysend(s,bytes(message_to_send, 'UTF-8'))
    elif clientMode == '2':
        try:
            print(s.recv(1024).decode('UTF-8'))
        except :
            if hit:
                change = ord(msvcrt.getch())
                if change == ord('q'):
                    mysend(s,bytes("left", 'UTF-8'))
                    connect = False
                elif change == ord('e'):
                    clientMode = '1'
                    mysend(s, bytes("mode" + clientMode, 'UTF-8'))
    else:
        print("your mode is false")
        connect = False



s.close()
print("finish")